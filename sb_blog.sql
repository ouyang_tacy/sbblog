/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50560
 Source Host           : localhost:3306
 Source Schema         : sb_blog

 Target Server Type    : MySQL
 Target Server Version : 50560
 File Encoding         : 65001

 Date: 05/04/2020 22:52:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员账户',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '昵称',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `status` int(10) DEFAULT 0 COMMENT '状态',
  `is_supper` int(10) DEFAULT 0 COMMENT '是否超级管理员',
  `create_time` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新时间',
  `delete_time` varbinary(100) DEFAULT NULL COMMENT '软删除时间',
  `isdel` int(10) DEFAULT 0 COMMENT '逻辑删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', 'admin', '超级管理员', 'admin@qq.com', 0, 1, '2020-02-28 13:30:11', NULL, NULL, 0);
INSERT INTO `admin` VALUES (2, 'zhangfei', 'admin', '张飞管理', 'zhangfei@qq.com', 0, 0, '2020-02-28 13:31:52', NULL, NULL, 0);
INSERT INTO `admin` VALUES (3, '最代码', '111111', 'zuidaima', 'zuidaima@qq.com', 0, 1, '2020-04-05 22:49:45', NULL, NULL, 0);

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `artdesc` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '概要',
  `tags` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标签',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '内容',
  `is_top` int(10) DEFAULT 0 COMMENT '是否推荐',
  `cate_id` int(10) NOT NULL COMMENT '所属导航id',
  `create_time` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新时间',
  `delete_time` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '软删除',
  `isdel` int(10) DEFAULT 0 COMMENT '软删除标记位',
  `member_id` int(10) DEFAULT NULL COMMENT '添加人员id',
  `viewnum` int(100) DEFAULT 0 COMMENT '浏览次数',
  `commentnum` int(100) DEFAULT 0 COMMENT '评论次数',
  `authorname` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '文章作者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES (1, 'SpringBoot框架原理', 'SpringBoot框架原理', 'Springboot|框架|原理', '<p>一、入门</p><p>1.简介</p><p>Spring Boot是一个简化Spring开发的框架。用来监护spring应用开发，约定大于配置，去繁就简，just run 就能创建一个独立的，产品级的应用。</p><p>我们在使用Spring Boot时只需要配置相应的Spring Boot就可以用所有的Spring组件，简单的说，spring boot就是整合了很多优秀的框架，不用我们自己手动的去写一堆xml配置然后进行配置。从本质上来说，Spring Boot就是Spring,它做了那些没有它你也会去做的Spring Bean配置。</p><p>2.优点</p><div><img width=\"485px\" src=\"https://ss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=899973693,1238735383&amp;fm=173&amp;app=49&amp;f=JPEG?w=485&amp;h=202&amp;s=1E8A762395C04CEA085DC0DB0100C0B0\"></div><p>3.单体应用与微服务</p><p>单体应用是把所有的应用模块都写在一个应用中，导致项目越写越大，模块之间的耦合度也会越来越高。微服务是一种架构风格，用微服务可以将应用的模块单独部署，对不同的模块进行不同的管理操作，不同的模块生成小型服务，每个功能元素最后都可以成为一个可以独立替换、独立升级的功能单元，各个小型服务之间通过http进行通信。</p><p>4.Spring Boot的核心特点</p><p>·微服务：</p><p>使用Spring Boot可以生成独立的微服务功能单元</p><p>·自动配置：</p><p>针对很多Spring应用程序常见的应用功能，Spring Boot能自动提供相关配置</p><p>·起步依赖：</p><p>告诉Spring Boot需要什么功能，它就能引入需要的库。</p><p>·命令行界面：</p><p>这是Spring Boot的可选特性，借此你只需写代码就能完成完整的应用程序，无需传统项目构建。</p><p>·Actuator：</p><p>让你能够深入运行中的Spring Boot应用程序。</p><p>5.简单案例：使用maven创建HelloWorld项目</p><div><img width=\"439px\" src=\"https://ss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=3842725662,3686477038&amp;fm=173&amp;app=49&amp;f=JPEG?w=439&amp;h=184&amp;s=6752E53A17BB40285E51B4CA0100C0B1\"></div><p>第一步：首先要配置Spring Boot 依赖</p><p>&lt;parent&gt;&lt;groupId&gt;org.springframework.boot&lt;/groupId&gt;&lt;artifactId&gt;spring-boot-starter-parent&lt;/artifactId&gt;&lt;version&gt;1.5.9.RELEASE&lt;/version&gt;&nbsp;&lt;/parent&gt;Spring Boot的版本仲裁中心；以后我们导入依赖默认是不需要写版本。（没有在dependencies里面管理的依赖自然需要声明版本号）&lt;dependencies&gt;&nbsp;&lt;dependency&gt;&lt;groupId&gt;org.springframework.boot&lt;/groupId&gt;&nbsp;&lt;artifactId&gt;spring-boot-starter-web&lt;/artifactId&gt;&nbsp;&lt;/dependency&gt;&nbsp;&lt;/dependencies&gt;</p><p>spring-boot-starter-web：</p><p>spring-boot-starter：spring-boot场景启动器。帮我们导入了web模块正常运行所依赖的组件。</p><p>Spring Boot将所有的功能场景都抽取出来，做成一个个的starters（启动器），只需要在项目里面引入这些starter 相关场景的所有依赖都会导入进来。要用什么功能就导入什么场景的启动器</p>', 0, 3, '2020-02-28 13:37:17', NULL, NULL, 0, 1, 1, 0, '大懒猫');
INSERT INTO `article` VALUES (2, 'Java & C++（引言一）', 'Java & C++（引言一）\r\n', 'java|c++', '<p>废话连篇</p><p>Java和C++都是世界范围内通用的两种语言，关于两种语言之间的差异和优劣的问题从来没有中断过。就差异问题而言，随着两种语言的不断成熟，差异性也越来越明显，各个大神都有各自的见解和说法，在这里我就不再赘述。就语言的优劣性而言，我认为语言之间没有优劣性而言，相信很多程序员的认知也是一样的，要不也不会有‘PHP是世界上最好的语言’这种无伤大雅的玩笑了，当然不可否认PHP确实是一门非常好的语言。其实我感觉任何语言之间都是有相通性的，无论一门语言被设计出来做什么，无论其编码风格，结构设计，都脱离不了‘以人为本’这个目的。</p><p>好了，废话不多讲，接下来直入正题。</p><div><img src=\"https://pics4.baidu.com/feed/8435e5dde71190ef982fc6c72571cd13fcfa6046.jpeg?token=476d1627084733f4d40a078af9d76710&amp;s=F9831B70600E774949E6554C0300E0F9\"></div><p>设计思想</p><p>就C++和Java而言，C++的设计能够让人更好的理解计算机，也能让计算机更好的理解人。当然，C++作为C的一个超集，为了向后兼容C，使语言本身的复杂度大大的增加，程序员在使用C++的时候不得不去花很多的时间去理清楚这层关系。但是这并不妨碍C++的效率性，反而为C++获得更高的效率提供了很大的帮助，一个好的程序员能够借助这些特点设计出很优美的程序，大大提高程序的执行效率和使用范围，当然这对程序员本身的要求是非常高的。这点在我看来，C++相对于Java来说，确实是要优美的多。</p><p>C++和Java都是一种杂合语言。之所以不说二者都是面向对象语言，是因为二者都不是纯粹的面向对象语言。（C++要向后兼容C的设计使得C++不是一个完全面向对象的语言。Java的八种基本数据类型和Java兼容C的设计，也使得Java不是一个纯粹的面向对象的语言，当然Java对八种数据类型提供了封装类，这点不说了。）但是面对Java时并不妨碍我们以一种绝对的面向对象的思想去考虑一个问题。Java的设计特点就是程序向自然万物的延伸，万物皆可被当作对象来看，缺什么程序员就创造什么。所以也有一种说法‘Java是一系列对象的集合’，这种说法确实没错。这种思想上完全面向对象的设计确实给Java带来了很大的便利，程序员不需要像在C++中一样考虑那么多关于程序底层的问题，上手非常快，所以选择Java作为第一语言的人非常的多。</p><div><img width=\"500px\" src=\"https://pics2.baidu.com/feed/0b46f21fbe096b63da2eb43be759d741ebf8ac12.jpeg?token=df36f987ae394a8345c86a2cd4568d76&amp;s=5EADA444C842FF5F802285830300E09B\"></div><p>物质守恒</p><p>在我看来，物质是守恒的，程序也一样需要考虑资源枯竭的问题。Java的程序员就像是一个造物主，程序本身是造物主的领地，领地内缺什么造物主就补充什么，不需要考虑资源枯竭的问题，因为程序底层的设计就是资源守恒的，没有用的时候就自动回收再利用。而C++的程序员则更像是一个领主，C++的程序本身是领主们的上帝，程序员想要改造自己的领地时需要向上帝申请资源，程序员用过这些资源后，必须还给上帝，保证上帝的资源守恒。</p><p>这一段说得比较绕，其实简单点就是垃圾回收和内存分配的问题。C++创建一个对象需要合理分配内存，分配存在时间。而Java完全不需要去考虑这两者，一切都可以自动完成，当然这样牺牲了很多效率，这也是Java执行效率低的一个主要原因。</p><p>有人说Java像是一个高配自动挡的汽车，开着舒服。C++像是全能手动挡的汽车，让人体会到操控的乐趣，就上面这两点来看确实是这样。</p><p>……未完待续……</p>', 0, 3, '2020-02-28 13:49:02', NULL, NULL, 0, 1, 1, 0, '空中飞船');
INSERT INTO `article` VALUES (3, '4亿美元，丰田投给了这家中国自动驾驶公司', NULL, '自动驾驶|丰田', '<p>北京时间2月26日，自动驾驶公司小马智行（Pony.ai) 宣布已从丰田汽车公司（Toyota Motor Corporation）筹集4亿美元，以加深和扩大两家公司在出行领域的合作。这是双方2019年建立的业务合作关系的延伸。在该基础上，两家公司将进一步加速自动驾驶的研发和商业化应用，旨在打造能造福每个人的产品和服务。</p><p>同时，小马智行也宣布其B轮融资总额高达4.62亿美元，估值略高于30亿美元。算上之前的融资，它的总融资额近8亿美元。这个成绩不仅再次打破中国自动驾驶公司估值的记录（上一次是该公司的Pre-B轮估值），也令小马智行成为全球最高估值的自动驾驶公司之一。</p><p>小马智行表示，此次合作关系的加深，有利于双方把一流的自动驾驶技术，与一流的车辆平台及技术更深入地融合，这和两家公司的战略重点一致。</p><div><img src=\"https://pics2.baidu.com/feed/d439b6003af33a8720e099036712573e5243b5eb.jpeg?token=52a633cb538bcda6de364fad48165b98&amp;s=460A2FE04C1225EF198D851B030040D1\"></div><p>据悉，小马智行和丰田于2019年8月宣布了共同在中国公开道路开始试点项目，使用丰田旗下雷克萨斯品牌的SUV，结合小马智行的自动驾驶的软件系统和硬件方案（上图为披露的自动驾驶车辆）。据悉，在今年的战略投资框架下，双方将扩大合作范围，除了共同开发自动驾驶技术外，双方还将在出行领域探索更多可能性。</p><p>2018年12月，小马智行发布PonyPilot自动驾驶移动出行（Robotaxi）项目，并在广州率先试点城区公开道路Robotaxi常态化运营，成为首个在中国市场提供Robotaxi服务的公司。2019年11月，小马智行在美国推出加州首个面向普通公众的Robotaxi服务，为其在全球范围内提供自动驾驶服务打下基础。根据公告，小马智行今后将与丰田携手，利用双方的技术、知识及服务，给人们带来安全和便利，共同推动移动社会的创建。</p>', 0, 4, '2020-02-28 13:50:09', NULL, NULL, 0, 1, 1, 0, '空中飞船');
INSERT INTO `article` VALUES (4, '何小鹏：李斌走了一步险棋，走得非常好，走得非常棒！', '何小鹏：李斌走了一步险棋，走得非常好，走得非常棒！\r\n', '测试|文章', '<div><img src=\"https://tukuimg.bdstatic.com/scrop/dea1e3949555d03c91dfa2e21135ad62.gif\"></div><div><img src=\"https://pics5.baidu.com/feed/8b82b9014a90f60366d3c83173eaf61db151ed67.jpeg?token=74b4e03ab070de5608a04d1887597c25&amp;s=C23AAA66C26332BCB236CC270300F04B\"></div><p>他在接受汽车商业评论采访时说：李斌也好，我也好，远远没马斯克苦！</p><div><img src=\"https://pics3.baidu.com/feed/c2fdfc039245d68836e1d9a8ed3a3818d01b24dd.jpeg?token=a1a3d62243ae48401563d6c41fd0be38&amp;s=1AA874234B6347245A5520DA000080B1\"></div><p>疫情还远远没有散去，中国车企复工也还在路上，但是造车新势力蔚来汽车最近却成为热点。</p><p>2月25日，蔚来汽车获得合肥市政府的巨大资金支持，双方签署了被外界认为不可能落空的框架协议。</p><p>汽车商业评论认为，这一方面是因为蔚来汽车本身的力量得到了地方政府的认可，一方面这也证明智能网联汽车的前景不可估量。</p><p>2月22日，征求意见达2年的《智能汽车创新发展战略》终于由国家11部委联合下发。虽然没有增加什么新内容，但至少表明国家对这一战略的正式认可，现在就看各地怎么落实了。</p><p>安徽省或者说合肥市开了一个好头，由地方政府真正投资靠谱的战略新兴产业是中国疫情过后拉动社会经济复苏的发动机。</p><p>汽车商业评论认为，虽然诸多造车新势力正在遇到麻烦，但是真正的头部造车新势力的价值正在得到体现。</p><p>“中国汽车产业正发生巨大变革，从功能汽车走向智能电动汽车。小鹏汽车将在2020年拿出优秀产品——全新旗舰产品小鹏P7上市，迎来智能电动汽车的新春天。”</p><p>2020年1月9日下午，小鹏汽车董事长何小鹏在接受汽车商业评论总编辑、汽场联合创始人贾可博士长篇访谈时如此说道。</p><div><img src=\"https://pics3.baidu.com/feed/d53f8794a4c27d1ee5320354522de868dcc43803.jpeg?token=2286dfe78104ea6ac570a317d916436c&amp;s=610592512CBB2D84708448F30300A0B2\"></div><p>当天，雪后的北京，寒风凛冽。何小鹏风尘仆仆地来到汽车商业评论的兄弟单位汽场买车APP的办公室。他和贾可就中国汽车的未来的诸多话题展开了讨论。比如：小鹏汽车能活下去吗？小鹏眼中的蔚来是什么样？何小鹏后悔造车吗？造车新势力的春天还远吗？</p><p>“李斌走了一步险棋，走得非常好，走得非常棒！”何小鹏说，“造车有很大的情怀，不然不会做这么苦逼的事。”</p><p>以下我们节录何小鹏对李斌和蔚来的看法和评价，以帮助大家更真实更真切了解造车新势力头部企业的想法和活法。关于此次访谈的全文，请关注2020年3月15日出版的汽车商业评论杂志。</p><p>“有些企业，在我和李斌看来，宁愿不做”</p><p>贾可：造车新势力，现在大家慢慢只关注头部的10家8家了，后面就不管了，不知道他们能不能活下去？</p><p>何小鹏：今年、明年就知道能不能活下去，现在10到8家没问题，未来就三四家。</p><p>贾可：后面就自动清盘了。</p><p>何小鹏：现在计划出车的不到10家，有人说出车，根本没出车，他根本没有钱或没有这个技术积累。就算今年会出车的，不外乎就是爱驰、理想。</p><p>贾可：还有天际、拜腾。</p><p>何小鹏：如果今年不能出车不能交付就糟糕了。之前出车的也有，但出不代表OK，我的意思是说，出车不代表能活，活着不代表能活好。</p><p>贾可：有一个企业我觉得很有趣，一共花了10亿元，可省钱了，融到一笔钱慢慢玩，慢慢死。</p><p>何小鹏：还有恒大、宝能都会出车的。其他还没有出车都完了。</p><p>贾可：还有一家领导做技术出身的企业，也挺悬的，我看也头疼。</p><p>何小鹏：如果我纯做互联网技术，我也干不了这事，在企业，技术只占四分之一。没有变化的管理不行。有些企业变化很快，核心点在平衡性，有很多企业做了很长时间，企业不变化。我有一个投资在美国失败了，是美国一个技术企业，5年10年跟以前没有什么变化，很稳定，不变。实际上创业就怕不变。</p><p>贾可：不变，抗挫折的能力就弱了。</p><p>何小鹏：如果是一个很大公司，很稳健，那是另外一回事。一个初创公司，二三十个人，5年10年后还是二三十人，你不痛苦？</p><p>贾可：那就是小卖部，就不想着做大，做大肯定不行。</p><p>何小鹏：最近5年，中国企业能走出去就走出去了，最近一两年能活下来，后面就起码不会死，但如果这两年活不下去就死掉了，这两年就特别明显。</p><p>贾可: 这两年得有钱续命。</p><p>何小鹏：有人认为是2019年，我不认为是，到2021年才看出来。</p><div><img src=\"https://pics2.baidu.com/feed/34fae6cd7b899e51ea327cb50a5f9c35ca950dd5.jpeg?token=5c14159f1170b75719fb3bc6658d1c09&amp;s=6702F70F6C4C46CE58906DE30300F027\"></div><p>图片来源：网络</p><p>贾可：2020年说撑就撑过去（当时还不知道这次突如其来的疫情）。</p><p>何小鹏：有些车企不死，它是在停摆，或者很缓慢地在摆，这种没有意义。如果这种企业，我们宁愿就关掉，仅仅是活着，活着没有意义。</p><p>贾可：最后就变成有些企业的样子，10年前就应该死掉，但那根管子不拔，植物人就不死。</p><p>何小鹏：他或者在想有哪个政府哪一天再救他一下，他就又活起来。</p><p>贾可：这种可能性已经没有了。</p><p>何小鹏：不知道有没有，不敢说一定有，现在连煤老板也没有钱了。</p><p>贾可：但可能还有人对汽车缺乏敬畏，对汽车认识不清。</p><p>何小鹏：换个角度，我不关注市场大小，我只关心谁能做出好的产品，谁能够走向全球，谁能够改变一些东西，仅仅是活着，没有意义。这个事情对我来说，没有价值。</p><p>贾可：你想改变这个行业。</p><p>何小鹏：否则我根本不会创业。我二次创业，如果让我活着去挣点钱，去赚3个亿，我投20多个亿去赚3个亿，我疯了？我根本不是为了这个目标去做的，所以，有些企业，在我和李斌看来，宁愿不做。</p><p>我们要做的事情，就是去做不一样的事情，不一样的硬件，不一样的软件，把服务运营做得不一样，可能这一次做不到，我们就分6步7步走，从短期效果或长期效果看。</p><p>一定要看长期效果，也要看短期效果，这是两种道理。换个角度说，很多企业融不到钱，也跟这个有关系——格局、野心、想象力。</p><p>包括李斌，包括我，还是有很大的情怀，不然不会做这么苦逼的事，你考虑我们的逻辑，根本不是说要做挣点钱能活下去的公司，那是完全两个思考的逻辑。</p><p>“李斌的服务做得很好，我真的是没想到”</p><p>贾可：现在有一部分造车新势力把一些车卖给自己的出行公司。</p><p>何小鹏：我不看重to B，我注重两个：一个是全球化，第二是差异化。但是卖给出行公司，卖给自己的出行公司，那没有意义，那是一个资本运作。</p><p>贾可：卖给外部出行公司可以吧？</p><p>何小鹏：卖给外部出行公司也有问题，有一个问题是品牌定位。有些新能源车企最大的痛苦在于，买我们（小鹏）车的人根本不拿我们的车对比那些新能源车企的产品。他们觉得，那是出租车。</p><p>很多的士公司来找我们：第一就是你要降价；第二你要终身质保。只要愿意，我都可以卖，这个地方卖500台，那个地方卖1000台，但你今年多卖的5000台、8000台，你明年的品牌就有可能下行，后年就很难了。而且你卖给他们，你就要不断地往下降价，你怎么做智能化？</p><p>中国汽车到了后面，如果不能品牌上行，就没有毛利，没有毛利就不会有研发，没有研发就不会有差异化，这是一个正向的循环。所以越是打性价比（牌），一定不会有差异，你不能有差异，你怎么能真正做强？</p><p>我们的车补贴前21万元，补贴后18万元起，我们一直很努力地在卖，前面很难。</p><p>贾可：现在就看你们的P7了。</p><div><img src=\"https://pics2.baidu.com/feed/4034970a304e251f46c586adef7e8c117e3e53b7.jpeg?token=19b865e6ae9d0650c37b507fc8083fc8&amp;s=E0358F744BE957036D4764D60300C0BA\"></div><p>图片来源：网络</p><p>何小鹏：P7很重要，是品牌往上，你看去年我的销售和市场费用，我可以看到蔚来的财报，我大概是他的十分之一。它在市场上也投了很多钱，销售、品牌、公关、市场、线索、销售、维修、服务都属于大销售体系，它是我的10倍。我今年开始发力，多花不少钱在这上面。</p><p>贾可：现在to B这方面在你看来，目前是旁门左道？</p><p>何小鹏：那不是，我从来没有这么说。我说的是如果做的士，或者偏低端的出行，对今天一个新的品牌价值不大，除非你的定位就在这里。一旦做了的士，目标就是规模、便宜、活着。</p><p>贾可：这是在中国的国情下。</p><p>何小鹏：如果你喊着要做中国智能汽车的引导者，做高端汽车，却去做的士，这是背离的。</p><p>贾可：你一直在强调运营用户这块，到现在为止，大家还做不到运营用户这个程度，还早着呢？</p><p>何小鹏：对。线下的叫服务，线上的叫运营，李斌的线下服务做得很好，但是能不能把成本控制到现在的三分之一，做到同样的效果，这个要看李斌的努力。我们现在在补很多服务的课。</p><p>我们去年做了很多运营的探索，我们有一些心得和有趣的效果。但受限于很多计算的能力，那个芯片我们很快就会有升级。今年，我们会把运营做得好一些，但是运营能力还做不到：第一赚很多钱，第二因为运营好，让客户买更多车。运营的效果在今天还没有体现。</p><p>贾可：运营，我们现在的手段实际还不多。</p><p>何小鹏：举一个例子，什么时候该开自动驾驶，什么时候该关掉，然后这个月充电和电耗费的情况，如果你的车被人撞了，或是你撞了别人的车，车会自动分析出来，然后打电话沟通该怎么修，这个都是全自动的。足够好的效果还没有体现，这个我说实话。</p><p>首先要把用户运营好才能商业。蔚来把服务做好，已经从推荐朋友那里获得很大的价值。李斌的服务做得很好，我真的是没想到。我以前做了很多服务，我不敢做这么好的服务，因为越大规模越麻烦。</p><p>服务是很难全球化的，服务一定是本土化的。用本土化的思维、本土化的文化去构建的服务的体验，它是跟人相关的，人就是区域化的。</p><p>“产品的科技能力是最能够全球化的”</p><p>贾可：蔚来是能挺过去的。</p><p>何小鹏：我也相信他能挺过去的，但是以什么方式挺过去，现在不敢说。</p><p>贾可：现在都还悬而未决，但是我认为他的底子还是很好的。</p><p>何小鹏：我也算是这个行业的人，但是我不知道，到底花多少现金是底子，我都不敢说。做车是一个长跑。</p><p>贾可：只要企业底子好，有现金流，就像人有血一样，哪怕这些血是借来的，就能把身体给弄好。</p><p>何小鹏：车需要的血太多，李斌走了一步险棋，走得非常好，走得非常棒。</p><div><img src=\"https://pics3.baidu.com/feed/d4628535e5dde71191788de7ef178b1d9c166173.jpeg?token=3bd445cf2aa7fa960f73c84b70c417bb&amp;s=37824DA3740004FE1C08E1B00300C002\"></div><p>图片来源：网络</p><p>贾可：现在感觉资本特别是风投不注重造车新势力了？</p><p>何小鹏：资本也注重，一年后两年后越来越好，能获得下一个资本接应，如果你做不到，很难的。</p><p>贾可：你自己投了多少？</p><p>何小鹏：我自己投了3个多亿元吧，我投得最多，李斌（对蔚来汽车）投了2亿多元。</p><p>贾可：如果现在不融钱，你能坚持多久，不知李斌能坚持多久？</p><p>何小鹏：李斌大概融了30多个亿元美金吧，实际上里面还是有债的。有的传统民营车企融不到这么多钱，所以没有办法做大的投入。像我P7花几十亿元就是为了把底盘、整个智能化全部从头做。这一点和我和李斌一样，苦什么都不能苦研发。一些传统民企做不了，我们做出来了，我们把品质做好，再优化，OTA几次，可能就会感觉差异很大。</p><p>这是一个新时代，新时代的核心就是智能汽车，智能汽车如果实现了电动，电动对用户的好感知和坏感知实际上都很强烈：好感知是加速快、安静；坏感知是成本贵、充电麻烦、续航不够。</p><p>到了第三代智能驾驶，后面就会越来越好，但是智能化会有多少人愿意买单，今天我还打个问号？我们的车主是非智能版不买，但是这个面有多广，体验有多好，这还需要数据去进一步认证。</p><p>贾可：自动驾驶，用户认知不太强烈的话，就不太会买单。</p><p>何小鹏：有人愿意买，但是加钱不多，这是最核心的问题。可能明年最晚后年就会有感觉，差异化很大。特斯拉以前说它有三大优势：自动驾驶、电动和超充。但它没有其他的智能，5G也没有，语音交互也没有，在中国国内，它的自动驾驶是真的不足。在硅谷它的自动驾驶真好，这个我要完全认。</p><p>换一个角度说，今天特斯拉的全球品牌声誉来自于它对科技的不断的新的变化的把握，各种各样的科技，没有一个是价格便宜，没有一个是请了明星宣传的，产品的科技能力是最能够全球化的。</p><p>贾可：现在你们自动驾驶芯片用英伟达的英伟达DRIVE Xavier，做自动驾驶你们研发得比较深，比较全乎。</p><p>何小鹏：如果从全球看，就属我们和特斯拉做自动驾驶最全乎。自动驾驶要到L3到L3.5差距才明显，L3.5到L4非常明显，你会看到技术对开车有巨大的变化，整个车都变化了，再过两年，等后年的今天，你会感觉不一样。</p><p>贾可：像李斌做的那个大电机，你做吗？</p><p>何小鹏：不做，特斯拉就是因为多做一个电机，多搞一个电芯厂，多干了一个芯片，多干了这几件事，那个电芯就花了几十亿美元，我哪能出那些钱？</p><p>“李斌也好，我也好，远远没他苦”</p><p>贾可：你对做车不后悔吧？</p><p>何小鹏：为什么后悔？现在是很痛，再过两三年就会开始有点快乐，很爽。</p><p>贾可：现在做车，就像绑在一个战车上，已经下不来了。</p><p>何小鹏：做企业都一样，做企业，人越多，股东越多，你的责任感就越强。这和开一个茶馆担负的责任感不一样。</p><p>贾可：现在你们整个多少人？</p><p>何小鹏：我们4000人左右。</p><p>贾可：今年准备交付多少？</p><p>何小鹏：去年是15000辆，去年我们交个8个月的车，还是一款车，今年两款车，明年就有三款车。</p><p>贾可：P7什么时候交付？</p><p>何小鹏：6月份。</p><p>贾可：你卖到多少，会觉得有点快乐？</p><p>何小鹏：我的核心点，第一是我卖了多少，第二是增速多少。卖到一年10万台，且增速比现在快很多，我觉得很开心。</p><p>你看即使前年底，马斯克卖到20万辆，还有无数人说他有问题，还有股价从300美元跌到200美元出头，中间他还说要私有化。实际上只有两个中东的老大给他说了意向而已，他就对外宣布了，然后被美国证监会惩罚，然后多惨，那时候他已经卖了20万台。</p><p>贾可：他一赌气说要私有化，那也说明他对前途是看好的。</p><p>何小鹏：没有。2009年他更困难，把房子都卖了，最后凑了2000万美金，顶进去，所以李斌也好，我也好，远远没他苦。</p><p>他也和我一样，是投资人，从2006年冲进去到现在，做了14年，我现在才冲进去2年，我2017年8月28号才进来，换个角度，他2009年最困难的时候，比我们困难10倍，他2014年进入中国卖了3000台，2015年进中国卖了7000多台，两年加在一起11000台，我去年一年就搞定了，当然他卖得难度比我高。</p><div><img src=\"https://pics3.baidu.com/feed/8cb1cb1349540923ec0ab417dba0940fb2de4972.jpeg?token=db2e9a8e80a69c2e536d57caba721fce&amp;s=C4A82AF790A9130DA091F07503004075\"></div><p>图片来源：网络</p><p>贾可：马斯克现在可能心里感觉很好？</p><p>何小鹏：不知道。你在外面看到他很好的时候，他可能很苦，因为他觉得他下一步的增长的速度不够，有可能，因为我不知道。</p><p>贾可：你觉得特斯拉度过危险期了吗？</p><p>何小鹏：不知道，这种话没有人敢说，再过三四年问这个问题，就是所有要进局要打牌的人都出了牌，最后就知道了。</p><p>贾可：现在是不是有一种对特斯拉盲目崇拜？</p><p>何小鹏：中国是当前结果论。比如说，有人把我们与特斯拉比，实际你应该拿特斯拉的第5年和我们的第5年去比，要不然没有可比性。特斯拉今年卖得好，但换个角度，它受了多大的苦？它是做得不错，也有很多未来的挑战。所以，我从来不会因为现在，就断言结果怎么样。</p><p>我一直说失败的企业有很多共性，成功的企业没有共性。不要去看别人做了这个，又去做那个，你也会成功，天时、地利与人和，要放到一起看。</p>', 0, 1, '2020-02-28 13:51:38', NULL, NULL, 0, 1, 1, 0, '空中飞船');
INSERT INTO `article` VALUES (5, 'springboot怎么学？', 'springboot怎么学？\r\n', '测试|文章', '<div>作者：码云 Gitee<br>链接：https://www.zhihu.com/question/50958416/answer/529875117<br>来源：知乎<br>著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。<br><br><div><p>学习 SpringBoot 是个需要付出大量时间和精力的过程，但其实上手了操作难度不大。为题主和广大想要学习 SpringBoot 的推荐下面几个开源项目，先从项目入手，希望对大家有所帮助。觉得不错请给开发者一个大大的赞(*￣︶￣)</p><p>1、<b>项目名称：</b>一个基于 SpringBoot 2 的管理后台系统 </p><figure><noscript><img src=\"https://pic3.zhimg.com/50/v2-d7385f31921d04772bbe532785f8b2a0_hd.jpg\"normal\" width=\"1305\"/></noscript><img src=\"https://pic3.zhimg.com/80/v2-d7385f31921d04772bbe532785f8b2a0_720w.jpg\" normal\"=\"\" width=\"1305\"></figure><p><b>项目介绍：</b>包含了用户管理，组织机构管理，角色管理，功能点管理，菜单管理，权限分配，数据权限分配，代码生成等功能， 系统基于 Spring Boot2.1 技术，前端采用了 Layui2.4 。数据库以MySQL/Oracle/Postgres/SQLServer 为实例，理论上是跨数据库平台 。</p><p><br></p><p><b>项目地址：</b><a href=\"https://link.zhihu.com/?target=https%3A//gitee.com/xiandafu/springboot-plus\" target=\"_blank\" rel=\"nofollow noreferrer\">https://gitee.com/xiandafu/springboot-plus</a></p></div></div>', 0, 2, '2020-02-28 13:52:49', NULL, NULL, 0, 1, 7, 2, '码云');
INSERT INTO `article` VALUES (6, '最代码官方验证通过', '该代码经过官方严格验证', '最代码', '<p>该代码经过官方严格验证，可以完美运行，请访问下载</p><p>更多代码请访问<a href=\"www.zuidaima.com\" target=\"_blank\">www.zuidaima.com</a></p>', 1, 5, '2020-04-05 22:45:11', NULL, NULL, 0, 1, 2, 1, '最代码');

-- ----------------------------
-- Table structure for cate
-- ----------------------------
DROP TABLE IF EXISTS `cate`;
CREATE TABLE `cate`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `catename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '导航名称',
  `sort` int(10) DEFAULT NULL COMMENT '排序',
  `create_time` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新时间',
  `delete_time` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '软删除',
  `isdel` int(10) DEFAULT 0 COMMENT '软删除标记',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cate
-- ----------------------------
INSERT INTO `cate` VALUES (1, '心灵鸡汤', 1, '2020-02-28 13:33:36', NULL, NULL, 0);
INSERT INTO `cate` VALUES (2, '学习心得', 1, '2020-02-28 13:34:05', NULL, NULL, 0);
INSERT INTO `cate` VALUES (3, '技术教程', 3, '2020-02-28 13:34:13', NULL, NULL, 0);
INSERT INTO `cate` VALUES (4, '轻松一刻', 2, '2020-02-28 13:34:31', NULL, NULL, 0);
INSERT INTO `cate` VALUES (5, '最代码', 5, '2020-04-05 22:43:36', NULL, NULL, 0);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '评论内容',
  `article_id` int(11) DEFAULT NULL COMMENT '评论文章id',
  `member_id` int(11) DEFAULT NULL COMMENT '评论用户id',
  `create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新时间',
  `delete_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '软删除时间',
  `isdel` int(10) DEFAULT 0 COMMENT '软删除标记位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (1, '这篇文章写得我都想哭了', 5, 1, '2020-02-28 13:53:42', NULL, NULL, 0);
INSERT INTO `comment` VALUES (2, '你这评论我都不敢接', 5, 2, '2020-02-28 13:54:48', NULL, NULL, 0);
INSERT INTO `comment` VALUES (3, '最代码官方验证过的代码', 6, 1, '2020-04-05 22:50:36', NULL, NULL, 0);

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '昵称',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮箱',
  `create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新时间',
  `delete_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '软删除时间',
  `isdel` int(10) DEFAULT 0 COMMENT '软删除标记位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES (1, 'zuidaima', '111111', '最代码官方', 'zuidaima@qq.com', '2020-04-05 13:53:18', NULL, NULL, 0);
INSERT INTO `member` VALUES (2, 'damahou', 'admin', '大马猴', 'damahou@qq.com', '2020-02-28 13:54:16', NULL, NULL, 0);
INSERT INTO `member` VALUES (3, '最代码客服', '111111', '最代码客服', 'zuidaima_noreply@qq.com', '2020-04-05 22:48:45', NULL, NULL, 0);
INSERT INTO `member` VALUES (4, '最代码牛牛', '111111', 'niuniu', 'zuidaima@qq.com', '2020-04-05 22:51:43', NULL, NULL, 0);

-- ----------------------------
-- Table structure for syssetting
-- ----------------------------
DROP TABLE IF EXISTS `syssetting`;
CREATE TABLE `syssetting`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `webname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '网站名称',
  `copyright` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '版权信息',
  `create_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '创建时间',
  `update_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '更新时间',
  `delete_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '软删除时间',
  `sign` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '个性签名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of syssetting
-- ----------------------------
INSERT INTO `syssetting` VALUES (1, '最代码', '京ICP备12032064号-2', '2020-02-28 13:33:06', '2020-04-05 22:47:59', NULL, '最代码 www.zuidaima.com 最全面,最专业的源代码分享网站');

SET FOREIGN_KEY_CHECKS = 1;
